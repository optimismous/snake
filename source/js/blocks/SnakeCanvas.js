var SnakeCanvas = function (params) {
	this.init(params);
};

SnakeCanvas.prototype = {
	init: function () {
		this.id = document.getElementById("snake-canvas");
        this.ctx = this.id.getContext('2d');

        this.colSize = (this.id.width / snakeGame.gameSize[0])|0;
        this.rowSize = (this.id.height / snakeGame.gameSize[1])|0;
	},

	fillRect: function (coords, color) {
		var x1 = this.colSize * coords.x,
			y1 = this.rowSize * coords.y;

		this.ctx.fillStyle = color;
		this.ctx.fillRect(x1, y1, this.colSize, this.rowSize);
	},

	clearLast: function (elem) {
		this.ctx.clearRect(this.colSize * elem[0], this.rowSize * elem[1], this.colSize, this.rowSize);
	}
}