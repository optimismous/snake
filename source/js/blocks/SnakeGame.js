var SnakeGame = function () {};

SnakeGame.prototype = {
	gameSize: [30, 20],
	foodCost: 5,
	score: 0,
	stopGame: function () {
		alert('Game over! Your score: ' + this.score);
		this.setLastScore();
	},

	changeScore: function () {
		this.score += this.foodCost;

		$('.snake-score__value').text(this.score);
	},

	foodGenerate: function () {
		var coords = {};
		this.foodCoords = [Math.floor(Math.random() * (this.gameSize[0])), Math.floor(Math.random() * (this.gameSize[1]))];

		coords = {
			x: this.foodCoords[0],
			y: this.foodCoords[1]
		};

		this.isCollision(coords) ? this.foodGenerate() : snakeCanvas.fillRect(coords, "orange");
	},

	isCollision: function  (coords) {
		for (var i = 0; i < snake.snakeCoordsArray.length; i++) {
			if (coords.x == snake.snakeCoordsArray[i][0] && coords.y == snake.snakeCoordsArray[i][1]) {
				return true;
			}
		};

		return false;
	},

	eatFood: function () {
		this.changeScore();
		this.foodGenerate();
	},

	getLastScore: function () {
		if (supports_html5_storage()) {
			var lastScore = localStorage.getItem('last_snake_score');

			if (lastScore) {
				$('.snake-last-score__value').text(lastScore);
			}
		}
	},

	setLastScore: function () {
		if (supports_html5_storage()) {
			localStorage.setItem('last_snake_score', this.score);
		}
	},

	init: function () {
		snake.init();
		this.foodGenerate();
		this.getLastScore();
		$(document).keydown(function (e) {
			if (e.keyCode <= 40 && e.keyCode >= 37) {
				snake.snakeTurn(e.keyCode);
			}
		})
	}
}