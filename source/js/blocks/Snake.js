var Snake = function () {};


Snake.prototype = {
	startLength: 5,
	direction: 39,
	directionArrays: {
		37: {
			x: -1,
			y: 0
		},
		38: {
			x: 0,
			y: -1
		},
		39: {
			x: 1,
			y: 0
		},
		40: {
			x: 0,
			y: 1
		}
	},

	init: function () {
		this.createStartSnake();

		this.startTimer();
	},

	startTimer: function () {
		var self = this;

		this.steper = setInterval(function () {
			self.snakeStep(false, self.directionArrays[self.direction]);
		}, 500);
	},

	createStartSnake: function () {
		var gameCenter = [Math.floor(snakeGame.gameSize[0]/2), Math.floor(snakeGame.gameSize[1]/2)],
			snakeHeadCoord;

		this.snakeCoordsArray = [];

		switch (this.direction) {
			case 37:
			case 39:
				snakeHeadCoord = [gameCenter[0] + Math.floor(this.startLength/2), gameCenter[1]];

				for (var i = 0; i < this.startLength; i++) {
					this.snakeCoordsArray.push([snakeHeadCoord[0] - i * this.directionArrays[this.direction].x, snakeHeadCoord[1]]);
				}

				break;

			case 38:
			case 40:
				snakeHeadCoord = [gameCenter[0], gameCenter[1] + Math.floor(this.startLength/2)];

				for (var i = 0; i < this.startLength; i++) {
					this.snakeCoordsArray.push([snakeHeadCoord[0], snakeHeadCoord[1] - i * this.directionArrays[this.direction].y]);
				}

				break;
		}

		for (var i = 0; i < this.snakeCoordsArray.length; i++) {
			var coords = {
				x: this.snakeCoordsArray[i][0], 
				y: this.snakeCoordsArray[i][1]
			};
			
			$('body').trigger('showSnake', coords);
		};
	},
	
	snakeStep: function (isEat, direction) {
		var coords = {};

		if (!isEat) {
			var last = this.snakeCoordsArray.pop();

        	snakeCanvas.clearLast(last);
        }


        this.snakeCoordsArray.unshift([this.snakeCoordsArray[0][0] + direction.x, this.snakeCoordsArray[0][1] + direction.y]);

        coords = {
        	x: this.snakeCoordsArray[0][0],
        	y: this.snakeCoordsArray[0][1]
        };

        $('body').trigger('showSnake', coords);

        this.checkCollision(coords);
	},

	checkCollision: function (coords) {
		for (var i = 1; i < this.snakeCoordsArray.length; i++) {
			if (coords.x == this.snakeCoordsArray[i][0] && coords.y == this.snakeCoordsArray[i][1]) {
				$('body').trigger('stopGame');
			};
		};

		if (coords.x < 0 || 
			coords.x > snakeGame.gameSize[0] ||
			coords.y < 0 || 
			coords.y > snakeGame.gameSize[1]) {

			$('body').trigger('stopGame');
		}

		if (this.snakeCoordsArray[0][0] == snakeGame.foodCoords[0] && this.snakeCoordsArray[0][1] == snakeGame.foodCoords[1]) {
			snakeGame.eatFood();

			this.snakeStep(true, this.directionArrays[this.direction]);
		}

	},
	snakeTurn: function (keycode) {
		if (keycode == this.direction || keycode == this.direction + 2 || keycode == this.direction -2) {
			return;
		} else {
			this.snakeStep(false, this.directionArrays[keycode]);
			this.direction = keycode;

		}
	}
}