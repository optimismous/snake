//= ../../../bower_components/jquery/dist/jquery.js
//= ../blocks/Snake.js
//= ../blocks/SnakeCanvas.js
//= ../blocks/SnakeGame.js
//= ../supportFunctions/support.js

$.fn.ready(function () {
	$('body').on('showSnake', function (event, coords) {
		snakeCanvas.fillRect(coords, 'green');
	});

	$('body').on('stopGame', function (event) {
		snakeGame.stopGame();
		clearInterval(snake.steper);
	});

	snakeGame = new SnakeGame();

	snakeCanvas = new SnakeCanvas();

	snake = new Snake();

	snakeGame.init();
});